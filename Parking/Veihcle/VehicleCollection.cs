﻿using Parking.Veihcle;
using System.Collections;

namespace Parking
{
    public class VehicleCollection : CollectionBase
    {
        public VehicleCollection()
        {

        }

        public BaseVehicle this[int index]
        {
            get { return (BaseVehicle)List[index]; }
            set { List[index] = value; }
        }

        public void Add(BaseVehicle vehicle)
        {
            List.Add(vehicle);
        }

        public void AddRange(VehicleCollection vehicleCollection)
        {
            foreach (var car in vehicleCollection)
            {
                List.Add(car);
            }
        }

        public void RemoveForIndex(int index)
        {
            InnerList.RemoveAt(index);
        }

        public bool RemoveForVehicle(BaseVehicle vehicle)
        {
            if (List.Contains(vehicle))
            {
                for (int i = 0; i < List.Count; i++)
                {
                    if(List[i] == vehicle)
                    {
                        List.RemoveAt(i);
                        return true;
                    }
                }
            }
            return false;
        }
    }
}