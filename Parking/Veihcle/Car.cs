﻿namespace Parking.Veihcle
{
    public class Car : BaseVehicle
    {
        public Car(string model, string number)
        {
            Model = model;
            Number = number;
        }
    }
}
